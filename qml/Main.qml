import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'xyz.peat-network.declarative-camera'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    DeclarativeCamera {
        anchors.fill: parent
    }
}
